using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class DeathScreen : MonoBehaviour
{
    [SerializeField] private Text _currentScore;
    [SerializeField] private Text _highScore;
    private void Start()
    {
        _currentScore.text = ScoreManager.Instance.score.ToString();
        _highScore.text = ScoreManager.Instance.HighScore().ToString();
    }
    public void RestartButton()
    {
        LevelManager.Instance.SceneOne();
    }
}
