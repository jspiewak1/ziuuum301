using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OptionController : MonoBehaviour
{
    public void CloseOption()
    {
        this.gameObject.SetActive(false);
    }
    public void CalibrationAccelerometer()
    {
        SaveData.SaveAccelerometer(Input.acceleration);
    }
}
