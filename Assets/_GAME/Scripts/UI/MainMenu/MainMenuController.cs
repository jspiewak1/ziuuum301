using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainMenuController : MonoBehaviour
{
    private void Start()
    {
        _optionPanel.SetActive(false);
    }
    public void StartGame()
    {
        LevelManager.Instance.SceneOne();
    }
    [SerializeField] private GameObject _optionPanel;
    public void Option()
    {
        _optionPanel.SetActive(true);
    }
    public void Quit()
    {
        LevelManager.Instance.Quit();
    }
}
