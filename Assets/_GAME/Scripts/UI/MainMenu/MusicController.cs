using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour
{
    private void Start()
    {
        if (SaveData.GetMusicPP()) //Je�eli MusicOn to przesuwak w prawo
        {
            OnActive();
        }
        else
        {
            OffActive();
        }
    }
    [SerializeField] private GameObject _musicOnObject;
    [SerializeField] private GameObject _musicOffObject;
    public void MusicOn()
    {
        SaveData.MusicOption(true);
        OnActive();
    }
    public void MusicOff()
    {
        
        SaveData.MusicOption(false);
        OffActive();
    }
    [SerializeField] private AudioCheck _audio;
    private void OnActive()
    {
        AudioCheck();
        _musicOnObject.SetActive(true);
        _musicOffObject.SetActive(false);
    }
    private void OffActive()
    {
        AudioCheck();
        _musicOnObject.SetActive(false);
        _musicOffObject.SetActive(true);
    }
    private void AudioCheck()
    {
        if (_audio != null)
        {
            _audio.AudioCheckM();
        }
    }
}
