using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pause : MonoBehaviour
{
    [SerializeField] private GameObject _pausePanel;
    private void Start()
    {
        ResumeTime();
    }
    public void PauseOnOff()
    {
        if (Time.timeScale==1)
        {
            StopTime();
        }
        else
        {
            ResumeTime();
        }
    }
    private void ResumeTime()
    {
        _pausePanel.SetActive(false);
        Time.timeScale = 1;
    }
    private void StopTime()
    {
        _pausePanel.SetActive(true);
        Time.timeScale = 0;
    }
    [SerializeField] private PlayerRotation _playRot;
    public void Calibration()
    {
        _playRot.Calibrate();
    }
}
