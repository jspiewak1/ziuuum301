using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class SpeedSlider : MonoBehaviour
{
    private Slider _speedSlider;
    public static float _speedAmount;
    private void Awake()
    {
        _speedSlider = GetComponent <Slider>();
    }
    private void Update()
    {
        _speedAmount = _speedSlider.value;
    }
}
