using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Lean.Pool;
public class RingCenter : MonoBehaviour, ITouch
{
    private RingParent _parent;
    public void Touch()
    {
        _parent = transform.parent.gameObject.GetComponent<RingParent>();
        _parent.NextPoint();
        ScoreManager.Instance.AddScore();
        this.gameObject.SetActive(false);
    }
}
