using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingParent : MonoBehaviour
{
    private int _ringAmount;
    private int _ringTaken = 0;
    private GameObject[] _rings;
    [SerializeField]private int _activeRingAtStart;
 
    public void NextPoint()
    {
        _ringTaken++;
        NewRing();
        if (_ringTaken >=_ringAmount) 
        {
            //Debug.Log("Wow Wygra�e� XD");
        }
    }
    private void Start()
    {
        _ringTaken = 0;
        _ringAmount = this.transform.childCount; // Saves amount of rings
        if (_activeRingAtStart > _ringAmount)
        {
            _activeRingAtStart = _ringAmount;
        }
        for (int i = 0; i < _ringAmount; i++)
        {
            this.gameObject.transform.GetChild(i).gameObject.SetActive(false); //turn off all children
        }
        for (int i = 0; i < _activeRingAtStart; i++)
        {
            NewRing();
        }
    }
    public void NewRing()
    {
        while (true)
        {
            int index = Random.Range(0, _ringAmount);
            if (this.gameObject.transform.GetChild(index).gameObject.activeSelf == false)
            {
                
                this.gameObject.transform.GetChild(index).gameObject.SetActive(true);
                break;
            }
            //Debug.Log("Reroll");
        }
    }
}
