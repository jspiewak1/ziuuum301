using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlaneTouch : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.GetComponent(typeof(ITouch)))
        {
        ITouch attribute = other.gameObject.GetComponent(typeof(ITouch)) as ITouch;
        attribute.Touch();
        }
        
    }
}
