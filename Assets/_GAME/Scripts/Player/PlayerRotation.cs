using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class PlayerRotation : MonoBehaviour
{
    [SerializeField] private Joystick _joystick;
    [SerializeField] private float _rotatePower = 1;
    private Vector3 _calib;
    private void Start()
    {
        _calib = SaveData.GetAccelerometer();
    }

    void Update()
    {
        Vector3 acceleration = Input.acceleration;
        acceleration -= _calib;
        Vector3 move = new Vector3(acceleration.y * _rotatePower, 0,-acceleration.x * _rotatePower);
        
        Vector3 moveJoy = new Vector3(-1*(Input.GetAxis("Vertical") + _joystick.Vertical) * _rotatePower, (Input.GetAxis("Horizontal") + _joystick.Horizontal) * _rotatePower, 0);
        transform.Rotate(moveJoy + move);        
    }
    public void Calibrate()
    {
        SaveData.SaveAccelerometer(Input.acceleration);
        _calib = SaveData.GetAccelerometer();
        Debug.Log(_calib);
    }
}
