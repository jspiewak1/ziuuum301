using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class SaveData 
{
    private static string accX = "accX";
    private static string accY = "accY"; 
    private static string accZ = "accZ";
    public static void SaveAccelerometer(Vector3 acc)
    {
        PlayerPrefs.SetFloat(accX, acc.x);
        PlayerPrefs.SetFloat(accY, acc.y);
        PlayerPrefs.SetFloat(accZ, acc.z);
    }
    public static Vector3 GetAccelerometer()
    {
        if (PlayerPrefs.HasKey(accX))
        {
        Vector3 temp = new Vector3(PlayerPrefs.GetFloat(accX), PlayerPrefs.GetFloat(accY), PlayerPrefs.GetFloat(accZ));
        return temp;
        }
        else
        {
            return Vector3.zero;
        }
    }
    public static string _musicPP = "Music";
    private static float _musicOn = 1;
    private static float _musicOff = 0;
    public static void MusicOption(bool music)
    {
        if (music) // Music ma by� w��czone
        {
            PlayerPrefs.SetFloat(_musicPP, _musicOn);
        }
        else
        {
            PlayerPrefs.SetFloat(_musicPP, _musicOff);
        }
    }
    public static bool GetMusicPP()
    {
        if (PlayerPrefs.HasKey(_musicPP)&& PlayerPrefs.GetFloat(_musicPP) == _musicOn)
        {
            return true;
        }
        return false;
    }

}
