using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVelocity : MonoBehaviour
{
    private Rigidbody _rb;
    [SerializeField] private float _speedAdded = 0;
    [SerializeField] private float _speedMultiplied = 0; //Change speed of speed change
    private float _brake = 0;

    private void Awake()
    {
        _rb = GetComponent<Rigidbody>();
    }
    private void Start()
    {
        StopDude();
    }
    void FixedUpdate()
    {
        _rb.MovePosition(transform.position + (_brake * transform.forward * Time.fixedDeltaTime *((_speedAdded + SpeedSlider._speedAmount)* _speedMultiplied)));
    }
    public void StopDude()
    {
        _brake = 0;
    }
    public void UnStopDude()
    {
        _brake = 1;
    }
    
}
