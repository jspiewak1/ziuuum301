using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioCheck : MonoBehaviour
{
    private AudioSource _audio;
    private void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }
    private void Start()
    {
        AudioCheckM();
    }
    public void AudioCheckM()
    {
        if (SaveData.GetMusicPP())
        {
            _audio.Play();
        }
        else
        {
            _audio.Stop();
        }
    }
}
