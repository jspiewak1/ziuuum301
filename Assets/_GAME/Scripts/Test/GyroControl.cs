using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GyroControl : MonoBehaviour
{
    private bool _gyroEnabled;
    private Gyroscope _gyro;
    private void Start()
    {
        _gyroEnabled = EnableGyro();
    }
    private bool EnableGyro()
    {
        if (SystemInfo.supportsGyroscope)
        {
            _gyro = Input.gyro;
            _gyro.enabled = true;
            return true;
        }
        return false; 
    }
    private void Update()
    {
        
    }
}
