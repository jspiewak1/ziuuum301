using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using TMPro;
public class GyroCoordinate : MonoBehaviour
{
    private TextMeshProUGUI _text;
    private void Awake()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }
    private void Update()
    {
        //_text.text = Input.gyro.attitude.eulerAngles.ToString();
        _text.text = Input.acceleration.ToString();
    }
}
