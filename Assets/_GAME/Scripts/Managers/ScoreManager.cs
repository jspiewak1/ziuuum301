using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
public class ScoreManager : MonoSingleton<ScoreManager>
{
    public float score;
    [SerializeField] private TextMeshProUGUI _scorePanel;
    private void Start()
    {
        score = 0;
        SendScoreToPanel();
    }
    public void AddScore()
    {
        
        score += ScoreCalculator();
        SendScoreToPanel();
    }
    private void SendScoreToPanel()
    {
        _scorePanel.text = score.ToString();
    }
    private float ScoreCalculator()
    {
        float i = SpeedSlider._speedAmount;
        i *= 10;
        i /= 3;
        i += 0.66f;
        i = Mathf.Round(i);
        return i;
    }
    private string highscore = "HighScore";
    public float HighScore()
    {
        if (PlayerPrefs.HasKey(highscore))
        {
            float hs = PlayerPrefs.GetFloat(highscore);
            if (hs >=score)
            {
                return hs;
            }
            //else
            //{
            //    PlayerPrefs.SetFloat(highscore, score);
            //}
        }
        PlayerPrefs.SetFloat(highscore, score);
        return score;
    }
}
