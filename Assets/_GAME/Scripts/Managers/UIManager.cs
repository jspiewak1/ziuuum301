using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoSingleton<UIManager>
{
    [SerializeField] private GameObject _deathScreen;
    [SerializeField] private PlayerVelocity _player;
    private void Start()
    {
        _deathScreen.SetActive(false);
    }
    public void DeathScreen()
    {
        _player.StopDude();
        _deathScreen.SetActive(true);
    }
}
