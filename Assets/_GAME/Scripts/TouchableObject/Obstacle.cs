using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour, ITouch
{

    public void Touch()
    {
        UIManager.Instance.DeathScreen();
    }
}
